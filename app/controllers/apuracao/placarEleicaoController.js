
app.controller('placarEleicaoController', ['$rootScope', '$scope', 'cidadeService', 'apuracaoVereadoresService', 'apuracaoPrefeitosService', 'apuracaoQuantitativoService', 'apuracaoGovernadoresService', 'apuracaoPresidentesService', '$location',
		function($rootScope, $scope, cidadeService, apuracaoVereadoresService ,apuracaoPrefeitosService, apuracaoQuantitativoService, apuracaoGovernadoresService, apuracaoPresidentesService, $location){

	$scope.cidades = $scope.cidade = {};
	$scope.candidate = $scope.candidates = {};
	$scope.presidente = $scope.presidentes = {};
	$scope.governador = $scope.governadores = {};
	$scope.deputado = $scope.deputados = {};
	$scope.prefeito = $scope.prefeitos = {};
	$scope.vereador = $scope.vereadores = {};
	$scope.quantitativo = $scope.quantitativos = {};
	$scope.results = {};
	$scope.tab = 1;
	$scope.pvv = 0;
	$scope.param = {
		cidade: {
			id: undefined
		},
		ano: 2016
	};

	$rootScope.$on('cidade', function(event, cidade) {
		$scope.cidade = cidade;
		var exists = 0;
		var url = $location.path();
		if(cidade!=undefined && $scope.param.ano == 2016){
			cidade.forEach(function(cidade){
				if(cidade.id==16){ 
					$scope.param.cidade.id=cidade.id;
					exists++;
				}
			});
		}else if($scope.param.ano == 2014){
			$scope.param.cidade.id = '99';
			$scope.param.cidade.nome = 'AC';
		}
		console.log($scope.param);
		if(exists && url=='/dashboard') $scope.filter();	
	});

	$scope.$on("governadores", function(event, governadores){
		$scope.governadores = governadores;
	});

	$scope.$on("presidentes", function(event, presidentes){
		switch($scope.tab){
			case 1:
				$scope.candidates = presidentes;
				$scope.presidentes = presidentes; 
			break;
			case 2:
				$scope.candidates = undefined;
			break;
		}
	});
	
	$scope.$on("prefeitos", function(event, prefeitos){
		$scope.prefeitos = prefeitos;
	});

	$scope.$on("quantitativos", function(event, quantitativos){
		$scope.quantitativos = quantitativos;
		switch(parseInt($scope.param.ano)){
			case 2014:
				$scope.pvv = $scope.quantitativos.results.governadores[0].nominais;
			break;
			case 2016:
				$scope.pvv = $scope.quantitativos.results.prefeitos[0].nominais;
			break;
		}
	});

	$scope.$on("vereadores", function(event, vereadores){
		$scope.totalItemsVereadores = vereadores.count.results;
		$scope.totalVotosVereadores = vereadores.count.votes;
		$scope.vereadores = vereadores;
	});

	$scope.$on("vereadores:loading", function(event, status){
	    $scope.results.loading = status;
	});

	$scope.loadAno = function(){
		switch(parseInt($scope.param.ano)){
			case 2014:
				$scope.param.cidade.id = '99';
				$scope.param.cidade.nome = 'AC';
				$scope.quantitativos = undefined;
			break;
			case 2016:
				$scope.param.cidade.id = '16';
				$scope.param.cidade.nome = 'Rio Branco';
			break;
		}
	};

	$scope.loadCidades = function(){
		cidadeService.getList();
	};

	$scope.setListCidade = function(){
		$scope.prefeitos = undefined;
		$scope.vereadores = undefined;
		$scope.quantitativos = undefined;
	}

	$scope.setTab = function(statusTab) {
		$scope.tab = statusTab;
		switch(parseInt($scope.param.ano)){
			case 2014:
				switch(statusTab){
					case 1:
						$scope.presidentes = undefined;
						$scope.presidente.status = statusTab;
						apuracaoPresidentesService.set($scope.presidente);
						apuracaoPresidentesService.getListPresidente2014_1turno();
					break;
					case 2:
						
					break;
				}
			break;
			case 2016:
				$scope.vereadores = undefined;
				$scope.vereador.status = statusTab;
				apuracaoVereadoresService.set($scope.vereador);
				apuracaoVereadoresService.getListVereadores();		
			break;
		}
		console.log($scope.param);
	}

	$scope.filter = function(){
		$scope.cidade.forEach(function(cidade){
			if(parseInt(cidade.id) == parseInt($scope.param.cidade.id))
				$scope.param.cidade = {
					id: cidade.id,
					nome: cidade.nome
				};
		});

		switch(parseInt($scope.param.ano)){
			case 2014:
				$scope.vereador.status = $scope.tab;
				$scope.presidente.cidade = $scope.param.cidade.nome;
				$scope.governador.cidade = $scope.param.cidade.nome;
		
				apuracaoPresidentesService.set($scope.presidente);
				apuracaoPresidentesService.getListPresidente2014_1turno();

				apuracaoGovernadoresService.set($scope.governador);
				apuracaoGovernadoresService.getList2014_1turno();
				apuracaoQuantitativoService.getList2014_1turno();

			break;
			case 2016:
				$scope.vereador.status = $scope.tab;
				$scope.vereador.cidade = $scope.param.cidade.nome;
				$scope.prefeito.cidade = $scope.param.cidade.nome;
				$scope.quantitativo.cidade = $scope.param.cidade.nome;
		
				apuracaoVereadoresService.set($scope.vereador);
				apuracaoVereadoresService.getListVereadores();
				apuracaoPrefeitosService.set($scope.prefeito);
				apuracaoPrefeitosService.getListPrefeitos();
				apuracaoQuantitativoService.set($scope.quantitativo);
				apuracaoQuantitativoService.getList2016_1turno();
			break;
		}
	}

	$scope.roundedPercentage = function(votes, totalvotes){
	   var result = (votes/totalvotes)*100;
	   return result.toFixed(2);
	}

	$scope.sum = function(value1, value2){
		var result = (parseInt(value1)+parseInt(value2));
		return result;
	}

}]);
